// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCombatCharacter.h"
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "PlayerCombatAnimationHandler.h"
#include "TwilightSoldierCharacter.generated.h"

//! Lock on Enum
UENUM(BlueprintType)
enum ELockOnStateReal
{
	LOCK_ON_REAL = 0,
	NO_LOCK_ON_REAL
};

UCLASS(config=Game)
class ATwilightSoldierCharacter : public ABaseCombatCharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	ATwilightSoldierCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	//! Shaun's variable
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LockOn")
	TEnumAsByte<ELockOnStateReal> lockOnStateReal;

	static bool tutorialDone;

	int total = 1;

protected:

	ComboType type;

	ComboNode* tmpAttackNode = nullptr;

	void GenerateCombo();
	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UFUNCTION(BlueprintCallable, Category = "AttackType")
	void LightAttack();

	UFUNCTION(BlueprintCallable, Category = "AttackType")
	void HeavyAttack();

	UFUNCTION(BlueprintCallable, Category = "AttackType")
	void PlayNextCombo();

	UFUNCTION(BlueprintCallable, Category = "AttackType")
	void ChangeWeaponRight();

	UFUNCTION(BlueprintCallable, Category = "AttackType")
	void ComboReset();

	UFUNCTION(BlueprintCallable, Category = "Tutorial")
	void SetTutorialDone(bool done);

	UFUNCTION(BlueprintCallable, Category = "Tutorial")
	bool GetTutorialDone() const;

	UFUNCTION(BlueprintCallable, Category = "UnlockWeapon")
	void UnlockWeapon();

	UFUNCTION(BlueprintCallable, Category = "WeaponTotal")
	int GetCurrentTotalWeapon() const;

	void WeaponSwitchCheck(ComboType changeType);

	UFUNCTION(BlueprintCallable, Category = "AttackType")
	void ChangeWeaponLeft();

	//! Shaun's function
	void LookUpAtRateNewReal(float Rate);
	UFUNCTION(BlueprintCallable, Category = "Camera")
	void TurnAtRateNewReal(float Rate);
};

