// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/*
 
 */

class ABaseCombatCharacter;

class TWILIGHTSOLDIER_API CombatAnimationHandler
{
public:
	CombatAnimationHandler();

	virtual ~CombatAnimationHandler();

	/** Sets the character that this combat animation handler belongs to.
	*   @param BaseCombatCharacter Pointer to the character
	*/
	void SetCharacter(ABaseCombatCharacter* BaseCombatCharacter);

	/** Called when in animation begin stage
	*/
	virtual void OnAnimationBegin() = 0;

	/** Called when in attack begin stage
	*/
	virtual void OnAttackBegin() = 0;

	/** Called when in attack end stage
	*/
	virtual void OnAttackEnd() = 0;

	/** Called when in animation endbegin stage
	*/
	virtual void OnAnimationEnd() = 0;

	//Called when animation is interrupted
	virtual void OnAnimationInterrupted() = 0;

	virtual void InputAvailable() = 0;

	FORCEINLINE bool IsAttacking() const { return isAttacking; }

	FORCEINLINE bool CanInput() const { return canInput; }

	

protected:

	/** Holds the character.
	*/
	ABaseCombatCharacter* Character;

	/** If true, the animations can be cancled
	*/
	bool canInterruptAttack;

	//Check if is attacking
	bool isAttacking;

	bool canInput;

public:
	bool isInputted = false;
};
