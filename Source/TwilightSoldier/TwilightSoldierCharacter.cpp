// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TwilightSoldierCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

//////////////////////////////////////////////////////////////////////////
// ATwilightSoldierCharacter

bool ATwilightSoldierCharacter::tutorialDone = false;

ATwilightSoldierCharacter::ATwilightSoldierCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 90.f;
	BaseLookUpRate = 90.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
	combatAnimationHandler = new PlayerCombatAnimationHandler();

	GenerateCombo();

	//! Shaun's stuff
	lockOnStateReal = NO_LOCK_ON_REAL;
}

void ATwilightSoldierCharacter::GenerateCombo()
{
	type = ComboType::SWORD;

	/*ComboNode* swordSwitch = new ComboNode("SwordSwitch", 100, ComboType::SWORD);
	ComboNode* swordSwitch2 = new ComboNode("SwordSwitch", 100, ComboType::SWORD);
	ComboNode* hammerSwitch = new ComboNode("AxeSwitch", 100, ComboType::HAMMER);
	ComboNode* hammerSwitch2 = new ComboNode("AxeSwitch", 100, ComboType::HAMMER);
	ComboNode* spearSwitch = new ComboNode("GSwordSwitch", 100, ComboType::SPEAR);
	ComboNode* spearSwitch2 = new ComboNode("GSwordSwitch", 100, ComboType::SPEAR);
	ComboNode* sword1 = new ComboNode("Sword1",10,ComboType::SWORD);
	ComboNode* sword2 = new ComboNode("Sword2", 20, ComboType::SWORD);
	ComboNode* sword3 = new ComboNode("Sword3", 30, ComboType::SWORD);
	ComboNode* hammerH1 = new ComboNode("SwitchAxe", 50, ComboType::HAMMER);
	ComboNode* spearH1 = new ComboNode("SwitchGreat", 50, ComboType::SPEAR);
	ComboNode* hammer1 = new ComboNode("Axe1", 40, ComboType::HAMMER);
	ComboNode* hammer2 = new ComboNode("Axe2", 40, ComboType::HAMMER);
	ComboNode* spear1 = new ComboNode("GSword1", 30, ComboType::SPEAR);
	ComboNode* spear2 = new ComboNode("GSword2", 30, ComboType::SPEAR);
	ComboNode* swordH1 = new ComboNode("SwitchSwordGS", 70, ComboType::SWORD);
	ComboNode* swordH2 = new ComboNode("SwitchSwordAxe", 60, ComboType::SWORD);

	hammer1->AddChildAttack(EAttackType::LIGHT_ATTACK, hammer2);
	hammer1->AddChildAttack(EAttackType::HEAVY_ATTACK, hammerH1);
	spear1->AddChildAttack(EAttackType::LIGHT_ATTACK, spear2);
	spear1->AddChildAttack(EAttackType::HEAVY_ATTACK, spearH1);
	sword1->AddChildAttack(EAttackType::LIGHT_ATTACK, sword2);
	sword1->AddChildAttack(EAttackType::HEAVY_ATTACK, swordH1);
	sword2->AddChildAttack(EAttackType::LIGHT_ATTACK, sword3);
	sword2->AddChildAttack(EAttackType::HEAVY_ATTACK, swordH2);

	SwordRoot->AddChildAttack(EAttackType::LIGHT_ATTACK, sword1);
	SwordRoot->AddChildAttack(EAttackType::SWITCH_ATTACK, hammerSwitch);
	SwordRoot->AddChildAttack(EAttackType::SWITCH_ATTACK_2, spearSwitch2);
	HammerRoot->AddChildAttack(EAttackType::LIGHT_ATTACK, hammer1);
	HammerRoot->AddChildAttack(EAttackType::SWITCH_ATTACK, spearSwitch);
	HammerRoot->AddChildAttack(EAttackType::SWITCH_ATTACK_2, swordSwitch2);
	SpearRoot->AddChildAttack(EAttackType::LIGHT_ATTACK, spear1);
	SpearRoot->AddChildAttack(EAttackType::SWITCH_ATTACK, swordSwitch);
	SpearRoot->AddChildAttack(EAttackType::SWITCH_ATTACK_2, hammerSwitch2);*/


	//Generate Sword Combo
	ComboNode* swordCombo1Key1 = new ComboNode("SwordCombo1Key1", 1.0f, ComboType::SWORD);
	ComboNode* swordCombo1Key2 = new ComboNode("SwordCombo1Key2", 1.0f, ComboType::SWORD);
	ComboNode* swordCombo1Key3 = new ComboNode("SwordCombo1Key3", 1.0f, ComboType::SWORD);
	ComboNode* swordCombo1Key4 = new ComboNode("SwordCombo1Key4", 1.0f, ComboType::SWORD);
	ComboNode* swordCombo1Key5 = new ComboNode("SwordCombo1Key5", 1.0f, ComboType::SWORD);

	ComboNode* swordCombo2Key2 = new ComboNode("SwordCombo2Key2", 1.0f, ComboType::SWORD);
	ComboNode* swordCombo2Key3 = new ComboNode("SwordCombo2Key3", 1.0f, ComboType::SWORD);
	ComboNode* swordCombo2Key4 = new ComboNode("SwordCombo2Key4", 1.0f, ComboType::SWORD);
	ComboNode* swordCombo2Key5 = new ComboNode("SwordCombo2Key5", 1.0f, ComboType::SWORD);

	//Generate Hammer Combo
	ComboNode* hammerCombo1Key1 = new ComboNode("HammerCombo1Key1", 1.0f, ComboType::HAMMER);
	ComboNode* hammerCombo1Key2 = new ComboNode("HammerCombo1Key2", 1.0f, ComboType::HAMMER);
	ComboNode* hammerCombo1Key3 = new ComboNode("HammerCombo1Key3", 1.0f, ComboType::HAMMER);
	ComboNode* hammerCombo1Key4 = new ComboNode("HammerCombo1Key4", 1.0f, ComboType::HAMMER);
	ComboNode* hammerCombo1Key5 = new ComboNode("HammerCombo1Key5", 1.0f, ComboType::HAMMER);

	ComboNode* hammerCombo2Key2 = new ComboNode("HammerCombo2Key2", 1.0f, ComboType::HAMMER);
	ComboNode* hammerCombo2Key3 = new ComboNode("HammerCombo2Key3", 1.0f, ComboType::HAMMER);

	//Generate Spear Combo
	ComboNode* spearCombo1Key1 = new ComboNode("SpearCombo1Key1", 1.0f, ComboType::SPEAR);
	ComboNode* spearCombo1Key2 = new ComboNode("SpearCombo1Key2", 1.0f, ComboType::SPEAR);
	ComboNode* spearCombo1Key3 = new ComboNode("SpearCombo1Key3", 1.0f, ComboType::SPEAR);
	ComboNode* spearCombo1Key4 = new ComboNode("SpearCombo1Key4", 1.0f, ComboType::SPEAR);
	ComboNode* spearCombo1Key5 = new ComboNode("SpearCombo1Key5", 1.0f, ComboType::SPEAR);
	ComboNode* spearCombo1Key6 = new ComboNode("SpearCombo1Key6", 1.0f, ComboType::SPEAR);
																  
	ComboNode* spearCombo2Key2 = new ComboNode("SpearCombo2Key2", 1.0f, ComboType::SPEAR);
	ComboNode* spearCombo2Key3 = new ComboNode("SpearCombo2Key3", 1.0f, ComboType::SPEAR);

	//Generate Swapping Strike Attack
	ComboNode* switchHammer = new ComboNode("EndSection", 1.0f, ComboType::HAMMER);
	ComboNode* switchSpear = new ComboNode("SwitchSpear", 1.0f, ComboType::SPEAR);
	ComboNode* switchSword = new ComboNode("EndSection", 1.0f, ComboType::SWORD);
	ComboNode* switchHammer2 = new ComboNode("EndSection", 1.0f, ComboType::HAMMER);
	ComboNode* switchSpear2 = new ComboNode("SwitchSpear", 1.0f, ComboType::SPEAR);
	ComboNode* switchSword2 = new ComboNode("EndSection", 1.0f, ComboType::SWORD);

	//Linking of Combo
	SwordRoot->AddChildAttack(EAttackType::SWITCH_ATTACK_RIGHT, switchSpear);
	SwordRoot->AddChildAttack(EAttackType::SWITCH_ATTACK_LEFT, switchHammer2);
	SwordRoot->AddChildAttack(EAttackType::LIGHT_ATTACK, swordCombo1Key1);
	SwordRoot->AddChildAttack(EAttackType::HEAVY_ATTACK, swordCombo2Key2);
	swordCombo2Key2->AddChildAttack(EAttackType::HEAVY_ATTACK, swordCombo2Key3);
	swordCombo2Key3->AddChildAttack(EAttackType::HEAVY_ATTACK, swordCombo2Key4);
	swordCombo2Key4->AddChildAttack(EAttackType::HEAVY_ATTACK, swordCombo2Key5);
	swordCombo1Key1->AddChildAttack(EAttackType::LIGHT_ATTACK, swordCombo1Key2);
	swordCombo1Key1->AddChildAttack(EAttackType::HEAVY_ATTACK, swordCombo2Key2);
	swordCombo1Key2->AddChildAttack(EAttackType::LIGHT_ATTACK, swordCombo1Key3);
	swordCombo1Key2->AddChildAttack(EAttackType::HEAVY_ATTACK, swordCombo2Key2);
	swordCombo1Key3->AddChildAttack(EAttackType::LIGHT_ATTACK, swordCombo1Key4);
	swordCombo1Key3->AddChildAttack(EAttackType::HEAVY_ATTACK, swordCombo2Key2);
	swordCombo1Key4->AddChildAttack(EAttackType::LIGHT_ATTACK, swordCombo1Key5);
	swordCombo1Key4->AddChildAttack(EAttackType::HEAVY_ATTACK, swordCombo2Key2);
	swordCombo1Key5->AddChildAttack(EAttackType::HEAVY_ATTACK, swordCombo2Key2);
	


	HammerRoot->AddChildAttack(EAttackType::LIGHT_ATTACK, hammerCombo1Key1);
	HammerRoot->AddChildAttack(EAttackType::HEAVY_ATTACK, hammerCombo2Key2);
	HammerRoot->AddChildAttack(EAttackType::SWITCH_ATTACK_RIGHT, switchSword);
	HammerRoot->AddChildAttack(EAttackType::SWITCH_ATTACK_LEFT, switchSpear2);
	hammerCombo2Key2->AddChildAttack(EAttackType::HEAVY_ATTACK, hammerCombo2Key3);
	hammerCombo1Key1->AddChildAttack(EAttackType::LIGHT_ATTACK,hammerCombo1Key2);
	hammerCombo1Key1->AddChildAttack(EAttackType::HEAVY_ATTACK, hammerCombo2Key2);
	hammerCombo1Key2->AddChildAttack(EAttackType::LIGHT_ATTACK, hammerCombo1Key3);
	hammerCombo1Key2->AddChildAttack(EAttackType::HEAVY_ATTACK, hammerCombo2Key2);
	hammerCombo1Key3->AddChildAttack(EAttackType::LIGHT_ATTACK, hammerCombo1Key4);
	hammerCombo1Key3->AddChildAttack(EAttackType::HEAVY_ATTACK, hammerCombo2Key2);
	hammerCombo1Key4->AddChildAttack(EAttackType::LIGHT_ATTACK, hammerCombo1Key5);
	hammerCombo1Key4->AddChildAttack(EAttackType::HEAVY_ATTACK, hammerCombo2Key2);
	hammerCombo1Key5->AddChildAttack(EAttackType::HEAVY_ATTACK, hammerCombo2Key2);
	

	SpearRoot->AddChildAttack(EAttackType::LIGHT_ATTACK, spearCombo1Key1);
	SpearRoot->AddChildAttack(EAttackType::HEAVY_ATTACK, spearCombo2Key2);
	SpearRoot->AddChildAttack(EAttackType::SWITCH_ATTACK_RIGHT, switchHammer);
	SpearRoot->AddChildAttack(EAttackType::SWITCH_ATTACK_LEFT, switchSword2);
	spearCombo2Key2->AddChildAttack(EAttackType::HEAVY_ATTACK, spearCombo2Key3);
	spearCombo1Key1->AddChildAttack(EAttackType::LIGHT_ATTACK, spearCombo1Key2);
	spearCombo1Key1->AddChildAttack(EAttackType::HEAVY_ATTACK, spearCombo2Key2);
	spearCombo1Key2->AddChildAttack(EAttackType::LIGHT_ATTACK, spearCombo1Key3);
	spearCombo1Key2->AddChildAttack(EAttackType::HEAVY_ATTACK, spearCombo2Key2);
	spearCombo1Key3->AddChildAttack(EAttackType::LIGHT_ATTACK, spearCombo1Key4);
	spearCombo1Key3->AddChildAttack(EAttackType::HEAVY_ATTACK, spearCombo2Key2);
	spearCombo1Key4->AddChildAttack(EAttackType::LIGHT_ATTACK, spearCombo1Key5);
	spearCombo1Key4->AddChildAttack(EAttackType::HEAVY_ATTACK, spearCombo2Key2);
	spearCombo1Key5->AddChildAttack(EAttackType::LIGHT_ATTACK, spearCombo1Key6);
	spearCombo1Key5->AddChildAttack(EAttackType::HEAVY_ATTACK, spearCombo2Key2);
	spearCombo1Key6->AddChildAttack(EAttackType::LIGHT_ATTACK, spearCombo1Key6);
	spearCombo1Key6->AddChildAttack(EAttackType::HEAVY_ATTACK, spearCombo2Key2);
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATwilightSoldierCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATwilightSoldierCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATwilightSoldierCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurningRate", this, &ATwilightSoldierCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATwilightSoldierCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ATwilightSoldierCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ATwilightSoldierCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ATwilightSoldierCharacter::OnResetVR);


	//! Shaun's stuff
	//PlayerInputComponent->BindAxis("TurnRate", this, &ATwilightSoldierCharacter::TurnAtRateNewReal);
	//PlayerInputComponent->BindAxis("LookUpRate", this, &ATwilightSoldierCharacter::LookUpAtRateNewReal);
}


void ATwilightSoldierCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ATwilightSoldierCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ATwilightSoldierCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ATwilightSoldierCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	UE_LOG(LogTemp, Warning, TEXT("Turn Rate %f"), InputComponent->GetAxisValue(TEXT("TurningRate")));
}

void ATwilightSoldierCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATwilightSoldierCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ATwilightSoldierCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}


void ATwilightSoldierCharacter::LightAttack()
{
	// Fetch the attack to perform
	tmpAttackNode = CurrentAttack->GetChildAttack(EAttackType::LIGHT_ATTACK);
	
	
	// If there is valid attack available with this input
	if (tmpAttackNode)
	{
		NextAttack = CurrentAttack->GetChildAttack(EAttackType::LIGHT_ATTACK);
	}
}

void ATwilightSoldierCharacter::HeavyAttack()
{
	// Fetch the attack to perform
	tmpAttackNode = CurrentAttack->GetChildAttack(EAttackType::HEAVY_ATTACK);
	

	// If there is valid attack available with this input
	if (tmpAttackNode)
	{
		NextAttack = tmpAttackNode;
	}
}

void ATwilightSoldierCharacter::ComboReset()
{
	CurrentAttack = ComboAttackRoot;
	NextAttack = nullptr;
}

int ATwilightSoldierCharacter::GetCurrentTotalWeapon() const
{
	return total;
}

void ATwilightSoldierCharacter::WeaponSwitchCheck(ComboType changeType)
{
	switch (changeType)
	{
	case ComboType::SWORD:
		ComboAttackRoot = SwordRoot;
		break;

	case ComboType::HAMMER:
		ComboAttackRoot = HammerRoot;
		break;

	case ComboType::SPEAR:
		ComboAttackRoot = SpearRoot;
		break;
	}
	CurrentAttack = ComboAttackRoot;
}

void ATwilightSoldierCharacter::ChangeWeaponLeft()
{
	tmpAttackNode = ComboAttackRoot->GetChildAttack(EAttackType::SWITCH_ATTACK_LEFT);

	if (tmpAttackNode) {
		NextAttack = tmpAttackNode;
	}
}

void ATwilightSoldierCharacter::PlayNextCombo()
{
	if (NextAttack && !combatAnimationHandler->IsAttacking())
	{
		if (NextAttack->GetAttackType() == EAttackType::SWITCH_ATTACK_RIGHT || NextAttack->GetAttackType() == EAttackType::SWITCH_ATTACK_LEFT)
		{
			int temp = 0;
			if (NextAttack->GetAttackType() == EAttackType::SWITCH_ATTACK_RIGHT)
			{
				temp = (int)type + 1;
			}
			else if (NextAttack->GetAttackType() == EAttackType::SWITCH_ATTACK_LEFT)
			{
				temp = (int)type - 1;
			}
			if (temp >= total)
			{
				temp -= total;
			}
			if (temp < 0)
			{
				temp = total - 1;
			}
			type = (ComboType)temp;
			if (CurrentAttack == ComboAttackRoot)
			{
				WeaponSwitchCheck(type);
				NextAttack = nullptr;
				return;
			}
			else
			{
				WeaponSwitchCheck(type);
			}
		}
		if (NextAttack != nullptr)
		{
			PlayAnimMontage(UpperBodyMontage, NextAttack->playRate, FName(*NextAttack->GetAttackTag()));
		}
		currentAttackType = NextAttack->GetAttackType();
		CurrentAttack = NextAttack;
		if (NextAttack->GetAttackType() == EAttackType::SWITCH_ATTACK_RIGHT || NextAttack->GetAttackType() == EAttackType::SWITCH_ATTACK_LEFT)
		{
			CurrentAttack = ComboAttackRoot;
		}
		NextAttack = nullptr;
	}
}

//! Shaun's function
void ATwilightSoldierCharacter::LookUpAtRateNewReal(float Rate)
{
	if (lockOnStateReal == NO_LOCK_ON_REAL)
	{
		BaseLookUpRate = 45;
	}
	else if (lockOnStateReal == LOCK_ON_REAL)
	{
		BaseLookUpRate = 20;
	}
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATwilightSoldierCharacter::TurnAtRateNewReal(float Rate)
{
	/*if (lockOnStateReal == NO_LOCK_ON_REAL)
	{
		// calculate delta for this frame from the rate information
		AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	}*/
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATwilightSoldierCharacter::ChangeWeaponRight()
{
	tmpAttackNode = ComboAttackRoot->GetChildAttack(EAttackType::SWITCH_ATTACK_RIGHT);
	
	if (tmpAttackNode)
	{
		NextAttack = tmpAttackNode;
	}
}

bool ATwilightSoldierCharacter::GetTutorialDone() const
{
	return ATwilightSoldierCharacter::tutorialDone;
}

void ATwilightSoldierCharacter::SetTutorialDone(bool done)
{
	ATwilightSoldierCharacter::tutorialDone = done;
}

void ATwilightSoldierCharacter::UnlockWeapon()
{
	total += 1;
}

