// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TwilightSoldierCharacter.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "ShaunCharacter.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum ELockOnState
{
	LOCK_ON = 0,
	NO_LOCK_ON
};

UCLASS()
class TWILIGHTSOLDIER_API AShaunCharacter : public ATwilightSoldierCharacter
{
	GENERATED_BODY()
public:
	AShaunCharacter();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void LookUpAtRateNew(float Rate);
	UFUNCTION(BlueprintCallable, Category = "Camera")
	void TurnAtRateNew(float Rate);

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LockOn")
		TEnumAsByte<ELockOnState> lockOnState;

private:
	
};
