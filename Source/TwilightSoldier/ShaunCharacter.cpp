// Fill out your copyright notice in the Description page of Project Settings.


#include "ShaunCharacter.h"

AShaunCharacter::AShaunCharacter()
{
	lockOnState = NO_LOCK_ON;
}

void AShaunCharacter::LookUpAtRateNew(float Rate)
{
	if (lockOnState == NO_LOCK_ON)
	{
		BaseLookUpRate = 45;
	}
	else if (lockOnState)
	{
		BaseLookUpRate = 20;
	}
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AShaunCharacter::TurnAtRateNew(float Rate)
{
	if (lockOnState == NO_LOCK_ON)
	{
		// calculate delta for this frame from the rate information
		AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	}
}

void AShaunCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AShaunCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AShaunCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AShaunCharacter::TurnAtRateNew);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AShaunCharacter::LookUpAtRateNew);
}