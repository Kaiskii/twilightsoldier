// Fill out your copyright notice in the Description page of Project Settings.


#include "ComboNode.h"

ComboNode::ComboNode(FString AttackTag, float rate,ComboType combo) : playRate(rate),AttackTag(AttackTag),Type(EAttackType::ROOT),comboType(combo)
{
}

ComboNode::~ComboNode()
{
	for (auto& attack : Branches)
	{
		delete attack.Value;
	}
}

void ComboNode::AddChildAttack(EAttackType AttackType, ComboNode* Attack, bool Forced)
{
	bool contains = Branches.Contains(AttackType);

	// If contains and force insert
	if (contains && Forced)
	{
		RemoveChildAttack(AttackType);
	}

	// Add the new attack
	if (!contains || (contains && Forced))
	{
		Attack->Type = AttackType;
		Branches.Add(AttackType, Attack);
	}
}

void ComboNode::RemoveChildAttack(EAttackType AttackType)
{
	bool contains = Branches.Contains(AttackType);
	if (contains)
	{
		// Free resource
		ComboNode* attack = Branches[AttackType];
		delete attack; // This should recursively delete all the child nodes.

		// Remove from map
		Branches.Remove(AttackType);
	}
}

ComboNode* ComboNode::GetChildAttack(EAttackType AttackType)
{
	bool contains = Branches.Contains(AttackType);
	if (contains)
	{
		return Branches[AttackType];
	}

	return nullptr;
}
