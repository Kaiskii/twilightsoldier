// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCombatAnimationHandler.h"

PlayerCombatAnimationHandler::PlayerCombatAnimationHandler()
{
}

PlayerCombatAnimationHandler::~PlayerCombatAnimationHandler()
{
}

void PlayerCombatAnimationHandler::OnAnimationBegin()
{
	isAttacking = true;
	canInput = false;
}

void PlayerCombatAnimationHandler::OnAttackBegin()
{
	canInterruptAttack = false;
}

void PlayerCombatAnimationHandler::OnAttackEnd()
{
	isAttacking = false;
}

void PlayerCombatAnimationHandler::OnAnimationEnd()
{

}

void PlayerCombatAnimationHandler::OnAnimationInterrupted()
{

}

void PlayerCombatAnimationHandler::InputAvailable()
{
	canInput = true;
}

void PlayerCombatAnimationHandler::RegisterInput()
{
	isInputted = true;
}
