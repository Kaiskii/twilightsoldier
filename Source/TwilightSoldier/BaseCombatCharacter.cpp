// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCombatCharacter.h"
#include "CombatAnimationHandler.h"

// Sets default values
ABaseCombatCharacter::ABaseCombatCharacter() : ComboAttackRoot(nullptr),CurrentAttack(ComboAttackRoot),combatAnimationHandler(nullptr)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SwordRoot = new ComboNode("", 100, ComboType::SWORD);
	HammerRoot = new ComboNode("", 100, ComboType::HAMMER);
	SpearRoot = new ComboNode("", 100, ComboType::SPEAR);
	ComboAttackRoot = SwordRoot;
	CurrentAttack = ComboAttackRoot;
	NextAttack = nullptr;

}

// Called when the game starts or when spawned
void ABaseCombatCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (combatAnimationHandler)
	{
		combatAnimationHandler->SetCharacter(this);
	}
}

// Called every frame
void ABaseCombatCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseCombatCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABaseCombatCharacter::OnAnimationBegin()
{
	if (combatAnimationHandler)
	{
		combatAnimationHandler->OnAnimationBegin();
	}
}

void ABaseCombatCharacter::OnAttackBegin()
{
	if (combatAnimationHandler)
	{
		combatAnimationHandler->OnAttackBegin();
	}
}

void ABaseCombatCharacter::OnAttackEnd()
{
	if (combatAnimationHandler)
	{
		combatAnimationHandler->OnAttackEnd();
	}
}

void ABaseCombatCharacter::OnAnimationEnd()
{
	CurrentAttack = ComboAttackRoot;

	if (combatAnimationHandler)
	{
		combatAnimationHandler->OnAnimationEnd();
	}
}

void ABaseCombatCharacter::InputAvailable()
{
	if (combatAnimationHandler)
	{
		combatAnimationHandler->InputAvailable();
	}
}

bool ABaseCombatCharacter::CanInput()
{
	if (combatAnimationHandler)
	{
		return combatAnimationHandler->CanInput();
	}
	return true;
}

ComboType ABaseCombatCharacter::GetCurrentComboType() const
{
	return CurrentAttack->GetComboType();
}

EAttackType ABaseCombatCharacter::GetCurrentAttackType() const
{
	return currentAttackType;
}

