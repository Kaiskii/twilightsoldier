// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine.h"
#include "MyWorldLocationToScreenLocation.generated.h"

/**
 * 
 */
UCLASS()
class TWILIGHTSOLDIER_API UMyWorldLocationToScreenLocation : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

		UFUNCTION(BlueprintPure, meta = (WorldContext = "WorldContextObject", CallableWithoutWorldContext), Category = "HUD|Util")
		static void FindScreenEdgeLocationForWorldLocation(UObject* WorldContextObject, const FVector& InLocation, const float EdgePercent, FVector2D& OutScreenPosition, float& OutRotationAngleDegrees, bool& bIsOnScreen);
		UFUNCTION(BlueprintPure, meta = (WorldContext = "WorldContextObject", CallableWithoutWorldContext), Category = "HUD|Util")
		static void WorldToScreenProjection(APlayerController* player ,FVector worldLocation,float xOffset,float yOffset, FVector2D& OutScreenPositionClamp, FVector2D& OutScreenPosition,bool& isBehindCamera);
};
