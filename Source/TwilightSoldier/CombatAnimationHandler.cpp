// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatAnimationHandler.h"

CombatAnimationHandler::CombatAnimationHandler() : canInterruptAttack(false),isAttacking(false),canInput(true)
{
}

CombatAnimationHandler::~CombatAnimationHandler()
{
}

void CombatAnimationHandler::SetCharacter(ABaseCombatCharacter* BaseCombatCharacter)
{
	Character = BaseCombatCharacter;
}
