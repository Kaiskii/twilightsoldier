// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "ComboNode.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCombatCharacter.generated.h"

class CombatAnimationHandler;

UCLASS()
class TWILIGHTSOLDIER_API ABaseCombatCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCombatCharacter();

	/** Holds the root node of the combo tree.
*/
	ComboNode* ComboAttackRoot;

	/** Holds the current attack that character is doing.
	*/
	ComboNode* CurrentAttack;

	//Hold the next attack animation
	ComboNode* NextAttack;

	ComboNode* SwordRoot;
	
	ComboNode* HammerRoot;

	ComboNode* SpearRoot;

protected:
	EAttackType currentAttackType;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Holds the animation montage.
*/
	UPROPERTY(EditDefaultsOnly, Category = "BaseCombatCharacter")
		UAnimMontage* UpperBodyMontage;

	/** Event fired from animation blueprint at animation begin stage
	*/
	UFUNCTION(BlueprintCallable, Category = "BaseCombatCharacter")
		void OnAnimationBegin();

	/** Event fired from animation blueprint at attack begin stage
	*/
	UFUNCTION(BlueprintCallable, Category = "BaseCombatCharacter")
		void OnAttackBegin();

	/** Event fired from animation blueprint at attack end stage
	*/
	UFUNCTION(BlueprintCallable, Category = "BaseCombatCharacter")
		void OnAttackEnd();

	/** Event fired from animation blueprint at animation end stage
	*/
	UFUNCTION(BlueprintCallable, Category = "BaseCombatCharacter")
		void OnAnimationEnd();

	UFUNCTION(BlueprintCallable, Category = "BaseCombatCharacter")
		void InputAvailable();

	UFUNCTION(BlueprintCallable, Category = "BaseCombatCharacter")
		bool CanInput();

	UFUNCTION(BlueprintCallable, Category = "BaseCombatCharacter")
		ComboType GetCurrentComboType() const;

	UFUNCTION(BlueprintCallable, Category = "BaseCombatCharacter")
		EAttackType GetCurrentAttackType() const;

	/** Holds the animation handler for this character.
	*/
	CombatAnimationHandler* combatAnimationHandler;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
